//
//  RegisterPage.swift
//  STAYaway
//
//  Created by Alex Grinberg on 08/12/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class RegisterPage: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        SVProgressHUD.show()
        
        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        view.bindtoKeyboard()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleScreenTap(sender:)))
        self.view.addGestureRecognizer(tap)
        
        SVProgressHUD.dismiss()
    }
    
    @objc func handleScreenTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func registerUser(_ sender:UIButton) {
        
        registerUser()
        
        
    }
    
    @IBAction func moveToLogin(_ sender:UIButton) {
        
        self.performSegue(withIdentifier: "LoginPage", sender: self)
    }
    
    func registerUser() {
 
        guard let name = nameTextfield.text, !name.isEmpty else { return }
        guard let email = emailTextfield.text, !email.isEmpty else { return }
        guard let password = passwordTextfield.text, !password.isEmpty else { return }
 
    Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error: Error?) in
 
            if let err = error {
                print("Failed to login:", err)
                return
        }
 
        print("Successfully created user:", user?.user.uid ?? "")
 
        let values = ["UID": user?.user.uid,"Name": name, "Email": email, "Password": password]
 
        Database.database().reference().child("HR_Users").child((Auth.auth().currentUser?.uid)!).updateChildValues(values as [AnyHashable : Any], withCompletionBlock: { (err, ref) in
 
            if let err = err {
            print("Failed to save user info into db:", err)

            } else {
                
                self.performSegue(withIdentifier: "MainPage", sender: self)
                }
 
            })
        })
    }
    

}
