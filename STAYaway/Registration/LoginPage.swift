//
//  LoginPage.swift
//  LawApp
//
//  Created by Alex Grinberg on 07/12/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class LoginPage: UIViewController, UITextFieldDelegate {
    
//    let uid = Auth.auth().currentUser?.uid
    
    //Login Outlets:
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    @IBOutlet weak var signIn: UIButton!
    
    @IBOutlet weak var loginView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()

        emailTextfield.delegate = self
        passwordTextfield.delegate = self
        view.bindtoKeyboard()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleScreenTap(sender:)))
        self.view.addGestureRecognizer(tap)
        
        SVProgressHUD.dismiss()
    }
    
    @objc func handleScreenTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func signinBtn(_ sender: Any) {
       SVProgressHUD.show()
        loginUser()
        SVProgressHUD.dismiss()
    }
    
     func loginUser() {
        
        guard let email = emailTextfield.text, !email.isEmpty else { return }
        guard let password = passwordTextfield.text, !password.isEmpty else { return }
        
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error: Error?) in
            
            if let err = error {
                print("Failed to login:", err)
                return
            }
            
            print("User Successfully Logged in:", user?.user.uid ?? "")
            
//            let vc = MainPage()
//            self.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func registerPage(_ sender: Any) {
        
        self.performSegue(withIdentifier: "RegisterPage", sender: self)

    }

    
    func createFirebaseDBUser(uid: String, userData: Dictionary<String, Any>, isVictim: Bool) {
        if isVictim {
            
            let ref = Database.database().reference().child(uid)
            ref.updateChildValues(userData)
        }
    }
    
    //Alert Function:
    func Alert(title: String, message: String) {
        
        let alert = UIAlertController(title: "Saved", message: "You medication was added", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (action) in
            
            self.dismiss(animated: true, completion: nil)
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}


