//
//  File.swift
//  LawApp
//
//  Created by Alex Grinberg on 07/12/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

extension UIView {
    func fadeTo(alphaValue: CGFloat, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration) {
            self.alpha = alphaValue
        }
    }
    
    func bindtoKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardWillChange(_ notification: NSNotification) {
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let curFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let targetFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let deltaY = targetFrame.origin.y - curFrame.origin.y
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY
        }, completion: nil)
    }
}

/*func registerUser() {
    
    guard let name = registerName.text, !name.isEmpty else { return }
    guard let email = registerEmail.text, !email.isEmpty else { return }
    guard let password = registerPassword.text, !password.isEmpty else { return }
    
    Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error: Error?) in
        
        if let err = error {
            print("Failed to login:", err)
            return
        }
        
        print("Successfully created user:", user?.user.uid ?? "")
        
        let values = ["UID": user?.user.uid,"Name": name, "Email": email, "Password": password]
        
        Database.database().reference().child("HR_Users").child((Auth.auth().currentUser?.uid)!).updateChildValues(values as [AnyHashable : Any], withCompletionBlock: { (err, ref) in
            
            if let err = err {
                print("Failed to save user info into db:", err)
                //return
            } else {
                self.performSegue(withIdentifier: "MainPage", sender: self)
            }
            
        })
    })
    
}
*/

