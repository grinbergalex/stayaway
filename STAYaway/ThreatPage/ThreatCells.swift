//
//  ThreatCells.swift
//  STAYaway
//
//  Created by Alex Grinberg on 08/12/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class ThreatCells: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
