//
//  ContactsSearchPage.swift
//  STAYaway
//
//  Created by Alex Grinberg on 08/12/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class ContactsSearchPage: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var threatsUsers = [Contacts]()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTab: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchContacts()

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return threatsUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThreatCells", for: indexPath) as! ThreatCells
        
        let info = threatsUsers[indexPath.row]
        cell.name.text = info.contactName
        cell.email.text = info.contactEmail
        
        return cell
        
    }
    
    func fetchContacts() {
        
        Database.database().reference().child("HR_Users").observe(DataEventType.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                let contact = Contacts()
                
                contact.contactName = (dictionary["Name"] as! String?)!
                contact.contactEmail = (dictionary["Email"] as! String?)!
                //user.profileImageURL = dictionary["profileImageURL"] as! String?
                
                self.threatsUsers.append(contact)
                
                DispatchQueue.main.async(execute: {
                    
                    self.tableView.reloadData()
                    
                })
                
                
            }
            
            print(snapshot)
            
        }, withCancel: nil)
        
    }
    
    
}
