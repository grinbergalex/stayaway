//
//  ViewController.swift
//  STAYaway
//
//  Created by Alex Grinberg on 07/12/2018.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import MapKit
import CoreLocation

class MapViewPage: UIViewController, CLLocationManagerDelegate,MKMapViewDelegate {
    
    var latitute = 32.047584
    var longitude = 34.799081
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var myLocation: UITextField!
    @IBOutlet weak var centerBtn: UIButton!
    
    @IBOutlet weak var locationStatus: UIView!
    @IBOutlet weak var safeStatus: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(currentLocation)
        
        
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        let span = MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitute, longitude: longitude), span: span)
        
        mapView.setRegion(region, animated: true)
        
        
        let pinLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        let pinObject = MKPointAnnotation()
        pinObject.coordinate = pinLocation
        
        pinObject.title = "Current Location"
        pinObject.subtitle = "Home"
        
        self.mapView.addAnnotation(pinObject)
    
        
        //Showing to user current location:
//        showMe()
        
    }
    
    @IBAction func showMyLocation(_ sender: UIButton) {
        
        showMe()
        
    }
    
    func locationSetup() {
        
            showMe()
    }


    @IBAction func addThreat(_ sender: UIButton) {
        
        let vc = ContactsSearchPage()
        
        tabBarController?.present(vc, animated: true, completion: nil)
    }
    
    func showMe() {
        let latitute = currentLocation.coordinate.latitude
        let longitude = currentLocation.coordinate.longitude
        
        let span = MKCoordinateSpan(latitudeDelta: 0.002, longitudeDelta: 0.002)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitute, longitude: longitude), span: span)
        
        mapView.setRegion(region, animated: true)
        
        
        let pinLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        let pinObject = MKPointAnnotation()
        pinObject.coordinate = pinLocation
        
        pinObject.title = "Current Location"
        pinObject.subtitle = "Home"
        
        self.mapView.addAnnotation(pinObject)
    }


}

